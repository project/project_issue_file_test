<?php

/**
 * @file
 * Provides extended_file_field integration for the PIFT module.
 */

/**
 * Implements hook_extended_file_field_output_alter().
 *
 * Modifies the extended_file_field table structure to add PIFT test results.
 */
function pift_extended_file_field_output_alter(&$elements, &$context) {
  // If we have not been passed any rows to add results to, then bail.
  if (empty($elements[0]['#rows'])) {
    return;
  }
  $items = $context['items'];
  $node = $context['entity'];
  $output_rows = array();
  $flip = array('odd' => 'even', 'even' => 'odd');
  $striping = 'odd';
  // Cycle through each row, adding PIFT details as needed.
  foreach ($elements[0]['#rows'] as $fid => $row) {
    // Group files with the same comment id, applying similar table striping.
    if (!empty($last_cid) && $row['data']['cid']['data'] != $last_cid) {
      $striping = $flip[$striping];
    }

    // Add the original row
    $output_rows[$fid] = $row;

    // Set no_striping to allow us to override table striping
    $output_rows[$fid]['no_striping'] = TRUE;
    $output_rows[$fid]['class'][] = $striping;

    $issue_wrapper = entity_metadata_wrapper('node', $node);
    if (pift_test_check_criteria_file((object) $items[$fid]) && pift_ci_project_info($issue_wrapper->field_project->raw())) {
      // Set a class and rowspan on the comment id column.
      if (!empty($row['data']['cid'])) {
        $output_rows[$fid]['data']['cid']['rowspan'] = 2;
      }

      // Add PIFT result details.
      $column_count = isset($row['data']) ? count($row['data']) : count($row);
      $file_tests = pift_load_tests($node, PIFT_CI_TYPE_FILE);
      $tests = pift_ci_display_tests($node, isset($file_tests[$fid]) ? $file_tests[$fid] : []);
      $output_rows[$fid . '-results'] = [
        'data' => [
          [
            'data' => drupal_render($tests),
            'colspan' => $column_count - 1,
          ],
        ],
        'no_striping' => TRUE,
        'class' => array_unique(array_merge(['extended-file-field-table-result-row', 'pift-test-info', $striping], $row['class'])),
      ];
    }

    // Set the $last_cid parameter for table striping
    $last_cid = !empty($row['data']['cid']['data']) ? $row['data']['cid']['data'] : NULL;
  }

  // Override the table rows with our updated element.
  $elements[0]['#rows'] = $output_rows;
}
