<?php

/**
 * @file
 * Provides general pages and UI functions for the PIFT module.
 */


/**
 * Form callback for per-project DrupalCI configuration.
 */
function pift_ci_project_form(stdClass $project_node) {
  $pift_ci_project = pift_ci_project_info($project_node->nid);

  // Find testable branch releases.

  $testable_branch_releases = [];
  foreach (project_release_query_releases($project_node->nid) as $release) {
    $release_wrapper = entity_metadata_wrapper('node', $release->nid);
    // Lets only display testing configurations for releases that are a. branches. and b. not obsolete.
    if ($release_wrapper->field_release_build_type->value() === 'dynamic' && $release_wrapper->field_release_category->value() != 'obsolete') {
      $testable_branch_releases[$release->nid] = $release;
    }
  }

  // If there are no testable_branch_releases, stop early.
  $output = [];
  if (empty($testable_branch_releases)) {
    $output['message'] = [
      '#prefix' => '<p>',
      '#markup' => t('Please create a dev release to enable automated testing.'),
      '#suffix' => '</p>',
    ];
    return $output;
  }

  if (pift_pages_project_testing_settings_access($project_node)) {
    $output['delete_all'] = [
      '#prefix' => '<p>',
      '#markup' => t('When GitLab CI is working, please <a href="!delete-all">delete all DrupalCI testing schedules</a>', [
        '!delete-all' => url('node/' . $project_node->nid . '/qa/delete_all', ['query' => ['destination' => 'node/' . $project_node->nid]]),
      ]),
      '#suffix' => '</p>',
    ];
  }

  $pift_ci_project_per_release = [];
  foreach ($pift_ci_project as $environment => $ci_releases) {
    foreach (array_keys($ci_releases) as $release_nid) {
      $pift_ci_project_per_release[$release_nid][] = $environment;
    }
  }
  $messages = [
    PIFT_CI_TEST_BRANCH_WEEKLY => t('tested weekly'),
    PIFT_CI_TEST_BRANCH_DAILY => t('tested daily'),
    PIFT_CI_TEST_BRANCH_COMMIT => t('tested on commit'),
    PIFT_CI_TEST_ISSUE => t('tested on commit, issue testing default'),
    PIFT_CI_TEST_DISABLED => t('disabled, testing configuration no longer available'),
  ];
  foreach (array_keys($testable_branch_releases) as $release_branch_nid) {
    $job_ids = [];

    if (isset($pift_ci_project_per_release[$release_branch_nid])) {
      foreach ($pift_ci_project_per_release[$release_branch_nid] as $environment) {
        foreach ($pift_ci_project[$environment][$release_branch_nid] as $core_branch => $trigger) {
          // Latest complete test.
          $query = new EntityFieldQuery();
          $query->entityCondition('entity_type', 'pift_ci_job')
            ->propertyCondition('target_type', PIFT_CI_TYPE_BRANCH)
            ->propertyCondition('release_nid', $release_branch_nid)
            ->propertyCondition('environment', $environment)
            ->propertyCondition('status', [PIFT_CI_STATUS_QUEUE, PIFT_CI_STATUS_SENT, PIFT_CI_STATUS_RUNNING], 'NOT IN')
            ->propertyCondition('custom', FALSE)
            ->propertyOrderBy('job_id', 'DESC')
            ->range(0, 1);
          // $core branch will only be empty if this is a drupal core test.
          if (!empty($core_branch)) {
            // Check current core branch used by release label.
            $query->propertyCondition('core_branch', pift_get_current_core($core_branch, $testable_branch_releases[$release_branch_nid]->field_release_category[LANGUAGE_NONE][0]['value']));

          }
          $result = $query->execute();
          if (isset($result['pift_ci_job'])) {
            $job_ids[array_keys($result['pift_ci_job'])[0]] = ['trigger' => $trigger, 'core_branch' => $core_branch];
          }
          elseif (!empty($core_branch)) {
            // Latest complete test, without specific core branch. For when the
            // value of a release label has recently changed, try to show
            // something.
            $query = new EntityFieldQuery();
            $query->entityCondition('entity_type', 'pift_ci_job')
              ->propertyCondition('target_type', PIFT_CI_TYPE_BRANCH)
              ->propertyCondition('release_nid', $release_branch_nid)
              ->propertyCondition('environment', $environment)
              ->propertyCondition('status', [PIFT_CI_STATUS_QUEUE, PIFT_CI_STATUS_SENT, PIFT_CI_STATUS_RUNNING], 'NOT IN')
              ->propertyCondition('custom', FALSE)
              ->propertyOrderBy('job_id', 'DESC')
              ->range(0, 1);
            $result = $query->execute();
            if (isset($result['pift_ci_job'])) {
              $job_ids[array_keys($result['pift_ci_job'])[0]] = ['trigger' => $trigger, 'core_branch' => $core_branch];
            }
          }
        }
      }

      // Queued tests.
      $query = new EntityFieldQuery();
      $query->entityCondition('entity_type', 'pift_ci_job')
        ->propertyCondition('target_type', PIFT_CI_TYPE_BRANCH)
        ->propertyCondition('release_nid', $release_branch_nid)
        ->propertyCondition('environment', $pift_ci_project_per_release[$release_branch_nid])
        ->propertyCondition('status', [PIFT_CI_STATUS_QUEUE, PIFT_CI_STATUS_SENT, PIFT_CI_STATUS_RUNNING])
        ->propertyCondition('custom', FALSE);
      $result = $query->execute();
      if (isset($result['pift_ci_job'])) {
        $job_ids += array_flip(array_keys($result['pift_ci_job']));
      }
    }

    // Recent custom tests.
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'pift_ci_job')
      ->propertyCondition('target_type', PIFT_CI_TYPE_BRANCH)
      ->propertyCondition('release_nid', $release_branch_nid)
      ->propertyCondition('custom', TRUE)
      ->propertyCondition('created', REQUEST_TIME - PIFT_CUSTOM_TIME, '>')
      ->propertyOrderBy('job_id', 'DESC');
    $result = $query->execute();
    if (isset($result['pift_ci_job'])) {
      $job_ids += array_fill_keys(array_keys($result['pift_ci_job']), ['trigger' => 'custom']);
    }

    // Render the tests.
    $output[$release_branch_nid]['#prefix'] = '<h3>' . check_plain($testable_branch_releases[$release_branch_nid]->field_release_version[LANGUAGE_NONE][0]['value']) . '</h3>';
    if (empty($job_ids)) {
      $output[$release_branch_nid]['tests'] = pift_ci_render_tests([]);
      $output[$release_branch_nid]['tests'][] = ['#markup' => t('No test results.')];
    }
    else {
      $jobs = entity_load('pift_ci_job', array_keys($job_ids));
      $output[$release_branch_nid]['tests'] = pift_ci_render_tests($jobs);
      $messages_displayed = [];
      $custom_jobs_displayed = [];
      foreach ($jobs as $job_id => $job) {
        if ($job_ids[$job->job_id]['trigger'] === 'custom') {
          // Do not show more than one custom job with the same environment,
          // release node ID, and core branch.
          if (isset($custom_jobs_displayed[$job->environment][$release_branch_nid][$job->core_branch])) {
            unset($output[$release_branch_nid]['tests'][$job_id]);
          }
          else {
            $custom_jobs_displayed[$job->environment][$release_branch_nid][$job->core_branch] = TRUE;
          }
        }
        elseif (isset($messages[$job_ids[$job->job_id]['trigger']]) && !isset($messages_displayed[$job->environment][$release_branch_nid][$job_ids[$job->job_id]['core_branch']][$job_ids[$job->job_id]['trigger']])) {
          $message = $messages[$job_ids[$job->job_id]['trigger']];
          $messages_displayed[$job->environment][$release_branch_nid][$job_ids[$job->job_id]['core_branch']][$job_ids[$job->job_id]['trigger']] = TRUE;
          if (pift_pages_project_testing_settings_access($project_node)) {
            $query = drupal_get_destination();
            $query['environment'] = $job->environment;
            if ($job_ids[$job->job_id]['trigger'] === PIFT_CI_TEST_DISABLED) {
              // Get the original non-disabled trigger.
              $query['schedule'] = pift_ci_project_info($project_node->nid, TRUE)[$job->environment][$release_branch_nid][$job_ids[$job->job_id]['core_branch']];
            }
            else {
              $query['schedule'] = $job_ids[$job->job_id]['trigger'];
            }
            $query['core_branch'] = $job_ids[$job->job_id]['core_branch'];
            $message .= ', ' . l(t('remove'), 'node/' . $project_node->nid . '/qa/add/' . $release_branch_nid, ['query' => $query]);
          }
          $output[$release_branch_nid]['tests'][$job_id]['#suffix'] = ' ' . $message . $output[$release_branch_nid]['tests'][$job_id]['#suffix'];
        }
      }
    }
  }

  return $output;
}

/**
 * Form callback. Delete tests for all branches.
 */
function pift_pages_project_testing_delete_all($form, &$form_state, stdClass $project) {
  drupal_set_title(t('Delete all DrupalCI schedules for @project', ['@project' => $project->title]));
  $form['intro'] = [
    '#prefix' => '<p>',
    '#markup' => t('Are you sure that you want to delete all schedules? <b>This operation cannot be undone.</b>'),
    '#suffix' => '</p>',
  ];
  $form['link_back'] = [
    '#markup' => l(t('Take me back'), 'node/' . $project->nid . '/qa'). ' '
  ];
  $form['submit'] = [
    '#type' => 'submit',
    '#value' => t('Delete all'),
  ];
  $form_state['#pift_delete_all'] = TRUE;

  return $form;
}

/**
 * Form submit callback for pift_pages_project_testing_delete_all().
 */
function pift_pages_project_testing_delete_all_submit($form, &$form_state) {
  if (!empty($form_state['#pift_delete_all'])) {
    db_delete('pift_ci_project')
      ->condition('nid', $form_state['build_info']['args'][0]->nid)
      ->execute();
    drupal_set_message(t('All the configured schedules were deleted.'));
  }
}

/**
 * Form callback. Retesting for branches.
 */
function pift_pages_project_testing_add($form, &$form_state, stdClass $project, stdClass $release) {
  $release_wrapper = entity_metadata_wrapper('node', $release);

  drupal_set_message(t('DrupalCI testing is deprecated. You cannot add new tests or edit existing ones.'), 'warning');

  // Check that this is a valid release.
  if (!project_release_node_is_release($release)) {
    return MENU_NOT_FOUND;
  }
  if (project_release_get_release_project_nid($release) != $project->nid) {
    return MENU_NOT_FOUND;
  }
  if ($release_wrapper->field_release_build_type->value() !== 'dynamic') {
    return MENU_NOT_FOUND;
  }

  $environments = variable_get('pift_ci_environments');
  $pift_ci_project = pift_ci_project_info($project->nid);

  // Custom job.
  $form['custom'] = array(
    '#tree' => TRUE,
    '#type' => 'fieldset',
  );
  // Environment.
  $form['custom']['environment'] = array(
    '#title' => t('Environment'),
    '#type' => 'select',
    '#options' => array(),
    '#disabled' => TRUE,
  );
  if (isset($_GET['environment'])) {
    $form['custom']['environment']['#default_value'] = $_GET['environment'];
  }

  $release_wrapper = entity_metadata_wrapper('node', $release);
  $category = $release_wrapper->field_release_category->value();
  foreach ($environments as $key => $environment) {
    $form['custom']['environment']['#options'][$key] = $environment['title'];
    if (!isset($form['custom']['environment']['#default_value'])) {
      $form['custom']['environment']['#default_value'] = $key;
    }
  }
  // Core.
  if ($project->type === 'project_core') {
    $form['custom']['core_branch'] = [
      '#type' => 'value',
      '#value' => '',
    ];
  }
  else {
    $form['custom']['core_branch'] = [
      '#title' => t('Core'),
      '#type' => 'radios',
      '#required' => TRUE,
      '#description' => l(t('About core branch labels'), 'drupalorg/docs/drupal-ci/drupal-core-branch-labels-in-drupalci'),
      '#disabled' => TRUE,
    ];
    $branch_labels = pift_get_core_labels();
    foreach ($branch_labels as $reference => $config) {
      if ($config->category == $category && $config->status) {
        $form['custom']['core_branch']['#options'][$config->machine_label] = $config->label . ', currently ' . $config->reference;
        $form['custom']['core_branch'][$config->machine_label]['#description'] = $config->label_description;
      }
    }
    if (isset($_GET['core_branch'])) {
      if (isset($form['custom']['core_branch']['#options'][$_GET['core_branch']])) {
        $form['custom']['core_branch']['#default_value'] = $_GET['core_branch'];
      }
    }
    else {
      list($form['custom']['core_branch']['#default_value']) = array_keys($form['custom']['core_branch']['#options']);
    }
  }
  // Schedule.
  $form['custom']['schedule'] = [
    '#title' => t('Schedule'),
    '#type' => 'radios',
    '#options' => [
      '' => t('Run once'),
      PIFT_CI_TEST_BRANCH_WEEKLY => t('Run weekly'),
      PIFT_CI_TEST_BRANCH_DAILY => t('Run daily'),
      PIFT_CI_TEST_BRANCH_COMMIT => t('Run on commit'),
      PIFT_CI_TEST_ISSUE => t('Run on commit and for issues'),
    ],
    '#default_value' => isset($_GET['schedule']) ? $_GET['schedule'] : '',
    '#disabled' => TRUE,
  ];
  foreach ($pift_ci_project as $environment => $project_info) {
    if (isset($project_info[$release->nid]) && in_array(PIFT_CI_TEST_ISSUE, $project_info[$release->nid])) {
      $form['custom']['schedule']['#options'][PIFT_CI_TEST_ISSUE] .= ', ' . t('replacing %environment', ['%environment' => $environments[$environment]['title']]);
      break;
    }
  }

  if (isset($_GET['environment']) && isset($_GET['core_branch']) && isset($_GET['schedule'])) {
    $form_state['#pift_updating'] = TRUE;
    $form['delete'] = [
      '#type' => 'submit',
      '#value' => t('Remove'),
    ];
  }

  return $form;
}

/**
 * Form submit callback for pift_pages_project_testing_add().
 */
function pift_pages_project_testing_add_submit($form, &$form_state) {
  // If updating, clear out old configuration.
  if (!empty($form_state['#pift_updating'])) {
    db_delete('pift_ci_project')
      ->condition('environment', $_GET['environment'])
      ->condition('core_branch', $_GET['core_branch'])
      ->condition('testing', $_GET['schedule'])
      ->condition('nid', $form_state['build_info']['args'][0]->nid)
      ->condition('release_nid', $form_state['build_info']['args'][1]->nid)
      ->execute();
  }

  if ($form_state['triggering_element']['#id'] === 'edit-delete') {
    drupal_set_message('Removed testing schedule.');
  }
}

/**
 * @return mixed
 */
function pift_get_core_labels() {
  $query = db_select('pift_ci_label_map', 'l');
  $query->join('field_data_field_release_category', 'fdfrc', 'fdfrc.entity_id = l.release_nid');
  $query->join('field_data_field_release_build_type', 'fdfrbt', 'fdfrbt.entity_id = l.release_nid');
  $query->fields('l', [
    'machine_label',
    'label',
    'label_description',
    'reference',
    'status',
    'release_nid',
  ]);
  $query->addField('fdfrc', 'field_release_category_value', 'category');
  $query->orderBy('fdfrbt.field_release_build_type_value', 'ASC');
  $query->orderBy('l.release_nid', 'ASC');
  $branch_label = $query->execute()->fetchAllAssoc('reference');
  return $branch_label;
}

/**
 * Menu callback, page for a PIFT CI job.
 */
function pift_ci_job_page(PiftCiJob $job) {
  // Load project* nodes and set breadcrumbs.
  $release_node = node_load($job->release_nid);
  $release_wrapper = entity_metadata_wrapper('node', $release_node);
  if (!empty($job->issue_nid)) {
    $issue_node = node_load($job->issue_nid);
    $project_node = node_load($issue_node->field_project[LANGUAGE_NONE][0]['target_id']);
    project_issue_set_breadcrumb($issue_node, $project_node);
    $breadcrumb = drupal_get_breadcrumb();
    $breadcrumb[] = l($issue_node->title, 'node/' . $issue_node->nid);
    drupal_set_breadcrumb($breadcrumb);
  }
  else {
    $project_node = node_load(project_release_get_release_project_nid($release_node));
    project_project_set_breadcrumb($project_node, TRUE);
    $breadcrumb = drupal_get_breadcrumb();
    $breadcrumb[] = l(t('Automated Testing'), 'node/' . $project_node->nid . '/qa');
    drupal_set_breadcrumb($breadcrumb);
  }
  drupal_set_title($job->getTitle(), PASS_THROUGH);

  $output = array(
    'meta' => array(
      '#prefix' => '<p class="submitted">',
      'created' => array(
        '#markup' => t('Created @created.', array('@created' => format_date($job->created))) . ' ',
      ),
      'reason' => array(
        '#markup' => t($job->reason, array(
          '!name' => theme('username', array('account' => user_load($job->uid))),
          '!bypass_name' => theme('username', array('account' => user_load($job->bypass_uid))),
          '@commit_id' => $job->commit_id,
          '@issue_branch' => $job->issue_branch,
        )) . ' ',
      ),
      'updated' => array(
        '#markup' => t('Updated @updated.', array('@updated' => format_date($job->updated))),
      ),
      '#suffix' => '</p>',
    ),
    'results_link' => array(
      '#prefix' => '<p>' . $job->message . ' ',
      '#suffix' => '</p>',
    ),
    'build_details' => array(),
    'cancel' => array(),
    'results' => array(),
    'checkstyle' => [],
    'branch' => array(),
    'history' => array(
      '#prefix' => '<h2>' . t('History') . '</h2>',
      '#theme' => 'table',
      '#header' => array(t('Updated'), t('Result')),
      '#empty' => t('No previous results.'),
      '#rows' => array(),
      '#attributes' => ['class' => ['pift-history']],
    ),
  );

  $output['results_link']['#markup'] = '<div class="warning">' . t('DrupalCI console output and artifacts are no longer available from dispatcher.drupalci.org. <a href="@link">Use GitLab CI instead</a>', [
    '@link' => url('docs/develop/git/using-gitlab-to-contribute-to-drupal/gitlab-ci'),
  ]) . '</div>';

  if ($job->status == PIFT_CI_STATUS_ERROR && (!empty($job->build_details))) {
    $output['build_details']['#markup'] = '<pre>' . check_plain(truncate_utf8(trim($job->build_details), 1000000, FALSE, TRUE)) . '</pre>';
  }

  // Related branch results.
  if (in_array($job->target_type, [PIFT_CI_TYPE_FILE, PIFT_CI_TYPE_MERGE_REQUEST])) {
    if ($branch_job = $job->getCurrentBranchJob()) {
      $output['branch']['current'] = pift_ci_render_tests([$branch_job]);
      $output['branch']['current'][$branch_job->job_id]['#suffix'] = ' ' . t('most recent, @date', array('@date' => format_date($branch_job->updated))) . $output['branch']['current'][$branch_job->job_id]['#suffix'];
    }
    if (($last_branch_job = $job->getLastBranchJob()) && $last_branch_job->job_id !== $branch_job->job_id) {
      $output['branch']['last'] = pift_ci_render_tests([$last_branch_job]);
      $output['branch']['last'][$last_branch_job->job_id]['#suffix'] = ' ' . t('as of issue test creation, @date', array('@date' => format_date($last_branch_job->updated))) . $output['branch']['last'][$last_branch_job->job_id]['#suffix'];
    }
    if (!empty($output['branch'])) {
      $output['branch']['#prefix'] = '<h2>' . t('@branch branch result', array('@branch' => $release_node->title)) . '</h2>';
    }
  }

  if ($job->status === PIFT_CI_STATUS_SWEPT) {
    $output['results'] = ['#markup' => '<p><em>' . t('Outdated results are no longer available.') . '</em></p>'];
  }
  else {
    // Results line items.
    $skipped_count = 0;
    foreach ($job->getResultArray() as $suite => $suite_results) {
      // If more than 1⁄3 of the memory is used at this point, there won't be
      // enough to show everything.
      if (!drupal_check_memory_limit(memory_get_usage() * 3)) {
        $skipped_count += array_sum(array_map('count', $suite_results));
        continue;
      }

      // Suite.
      $output['results']['#rows'][$suite] = array(
        'data' => array(
          PIFT_CI_RESULT_PASS => array(
            'data' => NULL,
            'class' => array('pift-ci-result-column'),
          ),
          PIFT_CI_RESULT_FAIL => array(
            'data' => NULL,
            'class' => array('pift-ci-result-column'),
          ),
          check_plain($suite),
        ),
        'class' => array('pift-ci-suite'),
        'no_striping' => TRUE,
      );

      foreach ($suite_results as $class => $class_results) {
        // Class.
        $output['results']['#rows'][$class] = array(
          'data' => array(
            PIFT_CI_RESULT_PASS => array(
              'data' => NULL,
              'class' => array('pift-ci-result-column'),
            ),
            PIFT_CI_RESULT_FAIL => array(
              'data' => NULL,
              'class' => array('pift-ci-result-column'),
            ),
            check_plain($class),
          ),
          'class' => array('pift-ci-class'),
          'no_striping' => TRUE,
        );

        foreach ($class_results as $job_result) {
          // Increment pass and fail sums.
          $output['results']['#rows'][$job_result->suite]['data'][$job_result->result]['data'] += 1;
          $output['results']['#rows'][$job_result->class]['data'][$job_result->result]['data'] += 1;

          // Test output.
          $text = '';
          if (!empty($job_result->output)) {
            $text .= '<pre>' . check_plain(truncate_utf8(trim($job_result->output), 1000000, FALSE, TRUE)) . '</pre>';
          }
          if (!empty($job_result->error)) {
            $text .= '<pre>' . check_plain(truncate_utf8(trim($job_result->error), 1000000, FALSE, TRUE)) . '</pre>';
          }

          $output['results']['#rows'][] = array(
            'data' => array(
              array(
                'data' => $job_result->result === PIFT_CI_RESULT_PASS ? '<span class="pift-ci-icon">✓</span>' : '',
                'class' => array('pift-ci-result-column'),
              ),
              array(
                'data' => $job_result->result === PIFT_CI_RESULT_FAIL ? '<span class="pift-ci-icon">✗</span>' : '',
                'class' => array('pift-ci-result-column'),
              ),
              array(
                'data' => empty($text) ? '<span class="pift-ci-no-output">-</span> ' . check_plain($job_result->test) : theme('ctools_collapsible', array(
                  'handle' => check_plain($job_result->test),
                  'content' => $text,
                  'collapsed' => $job_result->result === PIFT_CI_RESULT_PASS,
                )),
              ),
            ),
            'class' => array('pift-ci-' . $job_result->result),
            'no_striping' => TRUE,
          );
        }
      }
    }
    if ($skipped_count > 0) {
      $output['results']['#rows'][] = [
        'data' => [
          [
            'data' => '<em>' . t('Too much test output, @skipped_count tests skipped.', ['@skipped_count' => number_format($skipped_count)]) . '</em>',
            'colspan' => 3,
          ],
        ],
        'class' => ['pift-ci-message'],
        'no_striping' => TRUE,
      ];
    }
    if (isset($output['results']['#rows'])) {
      foreach ($output['results']['#rows'] as &$result_row) {
        if ($result_row['class'][0] === 'pift-ci-suite') {
          if (empty($result_row['data']['fail']['data'])) {
            $result_row['class'][] = 'pift-ci-all-pass';
          }
        }
        elseif ($result_row['class'][0] === 'pift-ci-class') {
          if ($all_pass = empty($result_row['data']['fail']['data'])) {
            $result_row['class'][] = 'pift-ci-all-pass';
          }
        }
        else {
          if ($all_pass) {
            $result_row['class'][] = 'pift-ci-all-pass';
          }
        }
      }
      // Add table information.
      $output['results']['#prefix'] = '<div class="pift-ci-toggle">' . t('Show: <a href="#" class="active">Failing classes</a> <a href="#">All</a>') . '</div>';
      $output['results']['#theme'] = 'table';
      $output['results']['#attributes'] = array(
        'class' => array('pift-ci-results', 'pift-ci-hide-all-pass'),
      );
      if ($job->result === PIFT_CI_RESULT_PASS) {
        array_unshift($output['results']['#rows'], array(
          'data' => array(
            array(
              'data' => '<em>' . t('All classes passed!') . '</em>',
              'colspan' => 3,
            ),
          ),
          'no_striping' => TRUE,
        ));
      }
      $output['results']['#header'] = array(
        array(
          'data' => t('✓'),
          'class' => array('pift-ci-result-column'),
        ),
        array(
          'data' => t('✗'),
          'class' => array('pift-ci-result-column'),
        ),
        NULL,
      );
    }
  }

  // Checkstyle results.
  if ($job->target_type === PIFT_CI_TYPE_BRANCH) {
    $files = [];
    $repository = versioncontrol_project_repository_load($project_node->nid);
    $vcs_branch = $repository->loadBranch($release_wrapper->field_release_vcs_label->value());
  }
  $n = 0;
  $last_file = '';
  $file_count = 0;
  if (isset($last_branch_job)) {
    $cs_diff_job = $last_branch_job;
  }
  elseif ($job->target_type === PIFT_CI_TYPE_BRANCH) {
    $query = new EntityFieldQuery();
    $result = $query->entityCondition('entity_type', 'pift_ci_job')
      ->propertyCondition('target_type', $job->target_type)
      ->propertyCondition('environment', $job->environment)
      ->propertyCondition('release_nid', $job->release_nid)
      ->propertyCondition('issue_nid', $job->issue_nid)
      ->propertyCondition('file_id', $job->file_id)
      ->propertyCondition('issue_branch', $job->issue_branch)
      ->propertyCondition('custom', $job->custom)
      ->propertyCondition('job_id', $job->job_id, '<')
      ->propertyOrderBy('job_id', 'DESC')
      ->range(0, 1)
      ->execute();
    if (isset($result['pift_ci_job'])) {
      $cs_diff_job = entity_load_single('pift_ci_job', array_keys($result['pift_ci_job'])[0]);
    }
  }
  $result = db_query('SELECT file, line, message FROM {pift_ci_job_checkstyle} WHERE job_id = :job_id ORDER BY file, line', [':job_id' => $job->job_id]);
  foreach ($result as $checkstyle_result) {
    if ($checkstyle_result->file !== $last_file) {
      if (isset($cs_diff_job)) {
        _pift_ci_job_page_checkstyle_diff($output, $last_file, $cs_diff_job, $file_count);
      }

      $last_file = $checkstyle_result->file;
      $file_count = 0;
      $output['checkstyle']['#rows'][$checkstyle_result->file] = [
        'data' => [
          [
            'data' => '<strong>' . check_plain($last_file) . '</strong>',
            'class' => ['pift-ci-file'],
            'id' => drupal_clean_css_identifier('cs-' . $last_file),
            'colspan' => 2,
          ],
        ],
        'no_striping' => TRUE,
      ];
    }
    $file_count += 1;
    $output['checkstyle']['#rows'][$n] = [
      'data' => [
        [
          'data' => $checkstyle_result->line,
          'class' => ['pift-ci-line-column'],
        ],
        check_plain($checkstyle_result->message),
      ],
      'no_striping' => TRUE,
    ];
    if (isset($files)) {
      $files['/' . $checkstyle_result->file][] = $n;
    }
    $n += 1;

    // If more than 1⁄3 of the memory is used at this point, there won't be
    // enough to show everything.
    if (!drupal_check_memory_limit(memory_get_usage() * 3)) {
      $count = 0;
      foreach ($result as $dummy) {
        $count += 1;
      }
      $output['checkstyle']['#rows'][] = array(
        'data' => [
          [
            'data' => '<em>' . t('Too much output, @count messages skipped.', ['@count' => number_format($count)]) . '</em>',
            'colspan' => 2,
          ],
        ],
        'class' => ['pift-ci-message'],
        'no_striping' => TRUE,
      );
    }
  }
  $checkstyle_title = format_plural($job->checkstyle, '1 coding standards message', '@count coding standards messages');
  $branch_diff = 0;
  if (isset($cs_diff_job)) {
    $branch_diff = _pift_ci_job_page_checkstyle_diff($output, $last_file, $cs_diff_job, $file_count);
  }
  if (isset($output['checkstyle']['#rows'])) {
    $output['checkstyle']['#prefix'] = '<h3><a class="pift-checkstyle-toggle">' . $checkstyle_title . '</a></h3>';
    $args = [
      '@diff' => abs($branch_diff),
      '!url' => url('pift-ci-job/' . $cs_diff_job->job_id),
      '@label' => $job->target_type === PIFT_CI_TYPE_BRANCH ? t('previous result') : t('branch result'),
    ];
    if ($branch_diff > 0) {
      $output['checkstyle']['#prefix'] .= '<p class="pift-ci-fail">' . t('<span class="pift-ci-icon">✗</span> @diff more than <a href="!url">@label</a>', $args) . '</p>';
    }
    elseif ($branch_diff < 0) {
      $output['checkstyle']['#prefix'] .= '<p class="pift-ci-pass">' . t('<span class="pift-ci-icon">✓</span> @diff fewer than <a href="!url">@label</a>', $args) . '</p>';
    }
    $output['checkstyle']['#theme'] = 'table';
    $output['checkstyle']['#sticky'] = FALSE;
    $output['checkstyle']['#attributes'] = [
      'class' => ['pift-ci-checkstyle'],
    ];
    // Empty heading to set fixed table layout widths.
    $output['checkstyle']['#header'] = [
      [
        'class' => ['pift-ci-line-column'],
      ],
      NULL,
    ];
    // Use the first line number as a heading.
    $output['checkstyle']['#rows'][0]['data'][0]['data'] = t('line !line', ['!line' => $output['checkstyle']['#rows'][0]['data'][0]['data']]);
  }
  elseif ($job->checkstyle) {
    // There are results which have been removed.
    $output['checkstyle']['#prefix'] = '<h3>' . $checkstyle_title . '</h3>';
    $output['checkstyle']['#suffix'] = '<p><em>' . t('Outdated coding standards messages are not available.') . '</em></p>';
  }

  // Link to previous results.
  $query = new EntityFieldQuery();
  $result = $query->entityCondition('entity_type', 'pift_ci_job')
    ->propertyCondition('target_type', $job->target_type)
    ->propertyCondition('environment', $job->environment)
    ->propertyCondition('release_nid', $job->release_nid)
    ->propertyCondition('issue_nid', $job->issue_nid)
    ->propertyCondition('file_id', $job->file_id)
    ->propertyCondition('issue_branch', $job->issue_branch)
    ->propertyCondition('custom', $job->custom)
    ->propertyOrderBy('job_id', 'DESC')
    ->execute();
  foreach (entity_load('pift_ci_job', array_keys($result['pift_ci_job'])) as $history_job) {
    $history_render = pift_ci_render_tests([$history_job]);
    $row = array(
      'data' => array(
        array('data' => format_date($history_job->updated)),
        array('data' => drupal_render($history_render)),
      ),
      'no_striping' => TRUE,
    );
    if ($history_job->updated === $job->updated) {
      $row['data'][0]['class'][] = 'active';
      $row['data'][1]['class'][] = 'active';
    }
    $output['history']['#rows'][] = $row;
  }

  return $output;
}

/**
 * Append difference with branch job to file heading, if there is one.
 */
function _pift_ci_job_page_checkstyle_diff(&$output, $last_file, $cs_diff_job, $file_count) {
  static $branch_count;
  static $branch_diff = 0;

  if ($branch_count !== FALSE && $last_file !== '') {
    if (!isset($branch_count)) {
      $branch_count = db_query('SELECT file, count(1) FROM {pift_ci_job_checkstyle} WHERE job_id = :job_id GROUP BY file', [':job_id' => $cs_diff_job->job_id])->fetchAllKeyed();
      if (empty($branch_count) && $cs_diff_job->checkstyle != 0) {
        // The branch job’s checkstyle results have been cleaned, there will be
        // no diff.
        $branch_count = FALSE;
        return FALSE;
      }
    }

    $diff = $file_count - (isset($branch_count[$last_file]) ? $branch_count[$last_file] : 0);
    $branch_diff += $diff;
    if ($diff > 0) {
      $output['checkstyle']['#rows'][$last_file]['data'][0]['data'] .= ' ' . l(t('<span class="pift-ci-icon">✗</span> @diff more', ['@diff' => abs($diff)]), 'pift-ci-job/' . $cs_diff_job->job_id, ['html' => TRUE, 'attributes' => ['title' => t('than branch result')], 'fragment' => drupal_clean_css_identifier('cs-' . $last_file)]);
      $output['checkstyle']['#rows'][$last_file]['data'][0]['class'][] = 'pift-ci-fail';
    }
    elseif ($diff < 0) {
      $output['checkstyle']['#rows'][$last_file]['data'][0]['data'] .= ' ' . l(t('<span class="pift-ci-icon">✓</span> @diff fewer', ['@diff' => abs($diff)]), 'pift-ci-job/' . $cs_diff_job->job_id, ['html' => TRUE, 'attributes' => ['title' => t('than branch result')], 'fragment' => drupal_clean_css_identifier('cs-' . $last_file)]);
      $output['checkstyle']['#rows'][$last_file]['data'][0]['class'][] = 'pift-ci-pass';
    }
  }

  return $branch_diff;
}
