(function ($) {
  // Toggle Failing classes / All.
  Drupal.behaviors.piftToggle = {
    attach: function (context, settings) {
      var $checkstyleToggle = $('.pift-checkstyle-toggle:not(.pift-checkstyle-toggle-processed)', context).addClass('pift-checkstyle-toggle-processed').click(function (e) {
        $('.pift-ci-checkstyle').toggle();
        e.preventDefault();
      });
      if (location.hash && location.hash.match(/^#cs-/)) {
        $checkstyleToggle.click();
      }

      $('.pift-ci-toggle:not(.pift-ci-toggle-processed) a', context).click(function (e) {
        if (!this.classList.contains('active')) {
          var $parent = $(this).parent();
          $parent.find('a').toggleClass('active');
          $parent.siblings('.pift-ci-results').toggleClass('pift-ci-hide-all-pass');
        }
        e.preventDefault();
      }).parent().addClass('pift-ci-toggle-processed');
    }
  };

  Drupal.behaviors.piftUpload = {
    attach: function (context, settings) {
      if (settings.file && settings.file.elements) {
        $.each(settings.file.elements, function(selector) {
          var extensions = settings.file.elements[selector];
          $(selector, context).bind('change', {extensions: extensions}, Drupal.pift.fileSelected);
        });
      }
    },
    detach: function (context, settings) {
      if (settings.file && settings.file.elements) {
        $.each(settings.file.elements, function(selector) {
          $(selector, context).unbind('change', Drupal.pift.fileSelected);
        });
      }
    }
  };

  Drupal.pift = Drupal.pift || {
    fileSelected: function (event) {
      if (Drupal.settings.piftRegex) {
        // Enable “Test with” field if the file matches piftRegex.
        $(this).parent().find("select[name*='[pift]']").attr('disabled', !(new RegExp(Drupal.settings.piftRegex)).test(this.value));
        // Select Do not test if filename matches.
        if (/do[-_]not[-_]test/.test(this.value)) {
          $(this).parent().find("select[name*='[pift]']").val(0);
        }
      }
    }
  };
})(jQuery);
