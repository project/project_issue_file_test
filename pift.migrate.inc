<?php

/**
 * @file
 * Project_Issue_File_Test D6 to D7 migration.
 */

/**
 * Migrate existing test data from the pift_test to pift_data table.
 *
 * Must run AFTER the project_issue migration has been completed, in order
 * to capture tests from files which were added to D6 comments.
 */
class ProjectIssueFileTestMigration extends MigrationBase {
  /**
   * General initialization of a ProjectIssueFileTestMigration object.
   */
  public function __construct() {
    parent::__construct();
    $this->description = t('Migrate pift_test data to the new pift_data table');
    $this->dependencies = array('ProjectIssueRebuildNodeFields');
  }

  protected function import() {
    if (db_table_exists('pift_data')) {
      db_drop_table('pift_data');
    }

    // Create the {pift_data} table if it doesn't already exist.
    $table = array(
      'description' => 'Stores test results.',
      'fields' => array(
        'test_id' => array(
          'description' => 'Unique test ID.',
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => FALSE,
        ),
        'type' => array(
          'description' => 'Type of test, PIFT_TYPE_*.',
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
        ),
        'id' => array(
          'description' => 'Related test detail record ID, either label_id, or fid.',
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
        ),
        'nid' => array(
          'description' => 'Related test nid information, either rid, or pi.nid.',
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => FALSE,
        ),
        'status' => array(
          'description' => 'Status of the test, PIFT_STATUS_*.',
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
        ),
        'message' => array(
          'description' => 'Summary message of test result.',
          'type' => 'text',
          'size' => 'big',
          'not null' => FALSE,
        ),
        'last_tested' => array(
          'description' => 'Timestamp when test results were last recieved.',
          'type' => 'int',
          'not null' => TRUE,
          'default' => 0,
        ),
      ),
      'primary key' => array('type', 'id', 'test_id'),
      'indexes' => array(
        'status' => array('status'),
        'last_tested' => array('last_tested'),
        'type_id' => array('type', 'id'),
        'type_nid' => array('type', 'nid'),
      )
    );
    // Create the table.
    db_create_table('pift_data', $table);

    // Copy data from the old {pift_test} table if it exists.
    if (db_table_exists('pift_test')) {
      // Build the branch tests sub_query.
      $query = db_select('pift_test', 'pt')
        ->condition('pt.type', 1);
      $query->join('versioncontrol_release_labels', 'vcs', 'pt.id = vcs.release_nid');
      $query->fields('pt', array('test_id', 'type', 'status', 'message', 'last_tested'));
      $query->addField('pt', 'id', 'nid');
      $query->addField('vcs', 'label_id', 'id');

      // Insert branch test records
      db_insert('pift_data')
        ->from($query)
        ->execute();

      // Build the files sub-query.
      $query = db_select('pift_test', 'pt');
      $query->join('file_usage', 'fu', 'pt.id = fu.fid');
      $query->groupBy('fu.fid');
      $query->fields('pt', array('test_id', 'type', 'status', 'message', 'last_tested'))
        ->condition('pt.type', 2);
      $query->addField('fu', 'id', 'nid');
      $query->addField('fu', 'fid', 'id');

      // Insert file test records
      db_insert('pift_data')
        ->from($query)
        ->execute();
    }
  }

  protected function rollback() {
    if (db_table_exists('pift_data')) {
      db_drop_table('pift_data');
    }
  }

  public function isComplete() {
    return db_table_exists('pift_data');
  }
}
