<?php
/**
 * @file
 * Drush integration functions for the PIFT module.
 */

/**
 * Implements hook_drush_command().
 */
function pift_drush_command() {
  $items = array();

  $items['pift-sweep-data'] = [
    'description' => 'Delete outdated results and data',
  ];
  $items['pift-disable-core-label'] = [
    'description' => 'Disable testing with a core label, and testing configurations using it.',
    'arguments' => [
      'label' =>  'Core label machine name, like “~development”',
    ],
  ];

  return $items;
}

/**
 * Delete outdated results and data.
 */
function drush_pift_sweep_data() {
  // Remove coding standards results from closed issues.
  if ($job_ids = db_query('SELECT DISTINCT jc.job_id FROM {pift_ci_job_checkstyle} jc INNER JOIN {pift_ci_job} j ON j.job_id = jc.job_id AND j.target_type IN (:targets) INNER JOIN {field_data_field_issue_status} fis ON fis.entity_id = j.issue_nid AND fis.field_issue_status_value NOT IN (:open)', [':targets' => [PIFT_CI_TYPE_FILE, PIFT_CI_TYPE_MERGE_REQUEST], ':open' => project_issue_open_states()])->fetchCol()) {
    drush_log(dt('Cleaning issue code style results: @ids', ['@ids' => implode(', ', $job_ids)]));
    db_delete('pift_ci_job_checkstyle')->condition('job_id', $job_ids)->execute();
  }

  // … and from custom branch tests that have dropped off the project’s page.
  if ($job_ids = db_query('SELECT DISTINCT jc.job_id FROM {pift_ci_job_checkstyle} jc INNER JOIN {pift_ci_job} j ON j.job_id = jc.job_id AND j.target_type = :target AND j.custom = 1 AND created < :custom_time', [':target' => PIFT_CI_TYPE_BRANCH, ':custom_time' => REQUEST_TIME - PIFT_CUSTOM_TIME])->fetchCol()) {
    drush_log(dt('Cleaning custom branch code style results: @ids', ['@ids' => implode(', ', $job_ids)]));
    db_delete('pift_ci_job_checkstyle')->condition('job_id', $job_ids)->execute();
  }

  // … and from non-custom branch tests that are older than 24 hours and are
  // not the most recent results.
  $result = db_query('SELECT j.release_nid, j.environment, max(j.job_id) latest_job_id FROM {pift_ci_job_checkstyle} jc INNER JOIN {pift_ci_job} j ON j.job_id = jc.job_id AND j.target_type = :target AND j.custom = 0 GROUP BY j.release_nid, j.environment', [':target' => PIFT_CI_TYPE_BRANCH]);
  foreach ($result as $row) {
    if ($job_ids = db_query('SELECT DISTINCT jc.job_id FROM {pift_ci_job_checkstyle} jc INNER JOIN {pift_ci_job} j ON j.job_id = jc.job_id AND j.target_type = :target AND j.custom = 0 AND created < :time AND j.release_nid = :release_nid AND j.environment = :environment AND j.job_id < :latest_job_id', [':target' => PIFT_CI_TYPE_BRANCH, ':time' => REQUEST_TIME - 24*60*60, ':release_nid' => $row->release_nid, ':environment' => $row->environment, ':latest_job_id' => $row->latest_job_id])->fetchCol()) {
      drush_log(dt('Cleaning branch code style results for release @release_nid @environment: @ids', ['@release_nid' => $row->release_nid, '@environment' => $row->environment, '@ids' => implode(', ', $job_ids)]));
      db_delete('pift_ci_job_checkstyle')->condition('job_id', $job_ids)->execute();
    }
  }

  // Remove all jobs from dispatcher that are older than 180 days.
  if ($result = db_query("SELECT DISTINCT pcj.job_id FROM {pift_ci_job} pcj WHERE pcj.updated < UNIX_TIMESTAMP(DATE_SUB(NOW(), INTERVAL 180 DAY)) AND pcj.ci_url != '';")) {
    foreach ($result as $jobid) {
      drush_log(dt('Deleted results from job #@job_id', ['@job_id' => $jobid->job_id]));
    }
  }

  // Remove all jobs from dispatcher that are passing branch tests that
  // have a newer passing test
  if ($result = db_query("SELECT DISTINCT pcj.job_id FROM {pift_ci_job} pcj
  LEFT JOIN (SELECT MAX(pcj.job_id) as 'job_id'
             FROM {pift_ci_job} pcj
             WHERE pcj.target_type = 'branch'
                   AND pcj.result = 'pass'
                   AND pcj.ci_url != ''
             GROUP BY pcj.release_nid, pcj.core_branch, pcj.environment ) AS pcj2 ON pcj2.job_id = pcj.job_id
WHERE pcj.target_type = 'branch'
      AND pcj.result = 'pass'
      AND pcj.ci_url != ''
      AND pcj2.job_id IS NULL
ORDER BY pcj.release_nid, pcj.job_id ASC;")) {
    foreach ($result as $jobid) {
      drush_log(dt('Deleted results from job #@job_id', ['@job_id' => $jobid->job_id]));
    }
  }

  // Remove all test results from dispatcher for closed/fixed issues
  if ($result = db_query("SELECT DISTINCT pcj.job_id FROM {pift_ci_job} pcj
  LEFT JOIN {field_data_field_issue_status} fdfis ON fdfis.entity_id = pcj.issue_nid
WHERE pcj.target_type = 'file'
      AND fdfis.field_issue_status_value IN (3,5,6,7,17,18)
      AND pcj.ci_url != ''
      AND pcj.issue_nid IS NOT NULL
      AND pcj.updated < UNIX_TIMESTAMP(DATE_SUB(NOW(), INTERVAL 30 DAY));")) {
    foreach ($result as $jobid) {
      drush_log(dt('Deleted results from job #@job_id', ['@job_id' => $jobid->job_id]));
    }
  }

  // Keep only the most recent RTBC patch job for a file/nid combo:
  if ($result = db_query("SELECT DISTINCT pcj.job_id FROM {pift_ci_job} pcj
  LEFT JOIN (SELECT MAX(pcj.job_id) as 'job_id'
             FROM {pift_ci_job} pcj
             WHERE pcj.target_type = 'file'
  AND pcj.reason = 'Automatic re-testing.'
  AND pcj.ci_url != ''
             GROUP BY pcj.issue_nid, pcj.file_id ) AS pcj2 ON pcj2.job_id = pcj.job_id
WHERE pcj.target_type = 'file'
  AND  pcj.reason = 'Automatic re-testing.'
  AND pcj.ci_url != ''
  AND pcj2.job_id IS NULL
ORDER BY pcj.release_nid, pcj.job_id ASC;")) {
    foreach ($result as $jobid) {
      drush_log(dt('Deleted results from job #@job_id', ['@job_id' => $jobid->job_id]));
    }
  }

  // Remove issue test results from local site for closed issues over 180 days.
  $job_ids = db_query("SELECT DISTINCT pcj.job_id FROM {pift_ci_job} pcj INNER JOIN {pift_ci_job_result} pcjr ON pcjr.job_id = pcj.job_id INNER JOIN {field_data_field_issue_status} fdfis ON fdfis.entity_id = pcj.issue_nid AND fdfis.field_issue_status_value NOT IN (:open) WHERE pcj.target_type = :target AND pcj.issue_nid IS NOT NULL AND pcj.updated < UNIX_TIMESTAMP(DATE_SUB(NOW(), INTERVAL 180 DAY))", [
    ':open' => project_issue_open_states(),
    ':target' => PIFT_CI_TYPE_FILE,
  ])->fetchCol();
  foreach (array_chunk($job_ids, 50) as $job_id_chunk) {
    drush_log(dt('Deleting local results from jobs @job_ids', ['@job_ids' => implode(',', $job_id_chunk)]));
    db_delete('pift_ci_job_result')->condition('job_id', $job_id_chunk)->execute();
    db_delete('pift_ci_job_result_diff')->condition('job_id', $job_id_chunk)->execute();
    foreach (entity_load('pift_ci_job', $job_id_chunk) as $job) {
      $job->status = PIFT_CI_STATUS_SWEPT;
      $job->save(FALSE, TRUE);
    }
  }

  // Remove custom branch test results that have dropped off the project’s
  // page.
  $job_ids = db_query("SELECT DISTINCT pcj.job_id FROM pift_ci_job pcj WHERE pcj.target_type = :target AND pcj.updated < :custom_time AND pcj.status != :status_swept AND pcj.custom = 1", [
    ':target' => PIFT_CI_TYPE_BRANCH,
    ':custom_time' => REQUEST_TIME - PIFT_CUSTOM_TIME,
    ':status_swept' => PIFT_CI_STATUS_SWEPT,
  ])->fetchCol();
  foreach (array_chunk($job_ids, 50) as $job_id_chunk) {
    drush_log(dt('Deleting local results from jobs @job_ids', ['@job_ids' => implode(',', $job_id_chunk)]));
    db_delete('pift_ci_job_result')->condition('job_id', $job_id_chunk)->execute();
    foreach (entity_load('pift_ci_job', $job_id_chunk) as $job) {
      $job->status = PIFT_CI_STATUS_SWEPT;
      $job->save(FALSE, TRUE);
    }
  }

  // Remove non-custom branch test results that are older than 180 days and are
  // not the most recent results, and do not have a dependant branch job.
  $result = db_query('SELECT j.release_nid, j.environment, max(j.job_id) latest_job_id FROM {pift_ci_job} j WHERE j.target_type = :target AND j.custom = 0 GROUP BY j.release_nid, j.environment', [
    ':target' => PIFT_CI_TYPE_BRANCH,
  ]);
  foreach ($result as $row) {
    $job_ids = db_query('SELECT DISTINCT pcj.job_id FROM {pift_ci_job} pcj LEFT JOIN {pift_ci_job} pcj_diff ON pcj.job_id = pcj_diff.result_diff_job_id AND pcj_diff.status != :status_swept WHERE pcj.target_type = :target AND pcj.updated < UNIX_TIMESTAMP(DATE_SUB(NOW(), INTERVAL 180 DAY)) AND pcj.status != :status_swept AND pcj_diff.result_diff_job_id IS NULL AND pcj.custom = 0 AND pcj.release_nid = :release_nid AND pcj.environment = :environment AND pcj.job_id < :latest_job_id', [
      ':target' => PIFT_CI_TYPE_BRANCH,
      ':status_swept' => PIFT_CI_STATUS_SWEPT,
      ':release_nid' => $row->release_nid,
      ':environment' => $row->environment,
      ':latest_job_id' => $row->latest_job_id,
    ])->fetchCol();
    foreach (array_chunk($job_ids, 50) as $job_id_chunk) {
      drush_log(dt('Deleting local results from jobs @job_ids', ['@job_ids' => implode(',', $job_id_chunk)]));
      db_delete('pift_ci_job_result')->condition('job_id', $job_id_chunk)->execute();
      foreach (entity_load('pift_ci_job', $job_id_chunk) as $job) {
        $job->status = PIFT_CI_STATUS_SWEPT;
        $job->save(FALSE, TRUE);
      }
    }
  }
}

/**
 * Disable testing with a core label, and testing configurations using it.
 */
function drush_pift_disable_core_label($label) {
  $count = db_query('SELECT count(1) FROM {pift_ci_project} WHERE core_branch = :label AND testing != :disabled', [
    ':label' => $label,
    ':disabled' => PIFT_CI_TEST_DISABLED,
  ])->fetchField();
  if (!drush_confirm(dt('@count project testing configurations will be disabled', ['@count' => $count]))) {
    return;
  }
  db_update('pift_ci_label_map')
    ->condition('machine_label', $label)
    ->fields(['status' => 0])
    ->execute();
  db_update('pift_ci_project')
    ->condition('core_branch', $label)
    ->fields(['testing' => PIFT_CI_TEST_DISABLED])
    ->execute();
}
