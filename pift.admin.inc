<?php

/**
 * @file
 * Administration pages for the PIFT module.
 */

/**
 * Form constructor for the PIFT settings form.
 *
 * @see pift_admin_settings_form_submit()
 *
 * @ingroup forms
 */
function pift_admin_settings_form($form, &$form_state) {
  $form = array();

  // Issue states come from the field_issue_status field.
  $field = field_info_field('field_issue_status');

  // User interface settings container.
  $form['interface'] = array(
    '#type' => 'fieldset',
    '#title' => t('User interface'),
    '#collapsible' => TRUE,
  );
  $form['interface']['pift_project_help'] = [
    '#type' => 'textarea',
    '#title' => t('Project “Automated testing” help'),
    '#description' => t('Appears on pages like node/{project}/qa.'),
    '#default_value' => variable_get('pift_project_help', ''),
  ];
  $form['interface']['pift_project_help_maintainer'] = [
    '#type' => 'textarea',
    '#title' => t('Additional project “Automated testing” help, for maintainers'),
    '#description' => t('Appears on pages like node/{project}/qa, for people with access to configure testing for the projet.'),
    '#default_value' => variable_get('pift_project_help_maintainer', ''),
  ];
  // Controls what to do with issue status when a test fails.
  $criteria = array(
    t('The file has failed testing.'),
    t('The issue the file is attached to is still passing the file testing criteria.'),
    t('The file is the last testable file posted to the issue.'),
  );
  $form['interface']['pift_followup_fail'] = array(
    '#type' => 'select',
    '#title' => t('Followup issue status - fail'),
    '#description' => t('The status to set an issue to when a test result meets all of the following criteria.' . theme('item_list', array('items' => $criteria))),
    '#default_value' => PIFT_FOLLOWUP_FAIL,
    '#options' => list_allowed_values($field),
    '#required' => TRUE,
  );
  // Controls what to do with issue status when a retest request is received.
  $form['interface']['pift_followup_retest'] = array(
    '#type' => 'select',
    '#title' => t('Followup issue status - retest'),
    '#description' => t('The status to set an issue to when someone requests a re-test and the issue currently does not fit the test criteria.'),
    '#default_value' => PIFT_FOLLOWUP_RETEST,
    '#options' => list_allowed_values($field),
    '#required' => TRUE,
  );

  $project_types = array();
  foreach (project_project_node_types() as $key => $project_type) {
    $project_types[$project_type] = $project_type;
  }
  $form['interface']['pift_project_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Project types'),
    '#description' => t('Project types for which the "Automated Testing" tab should be displayed on their project page.'),
    '#options' => $project_types,
    '#default_value' => variable_get('pift_project_types', array()),
  );

  // File Test Criteria settings container
  $form['criteria'] = array(
    '#type' => 'fieldset',
    '#title' => t('Criteria'),
    '#collapsible' => TRUE,
  );
  // Defines the regular expression file mask criteria.
  $form['criteria']['pift_regex'] = array(
    '#type' => 'textfield',
    '#title' => t('File regex'),
    '#description' => t('Full regex pattern that a filename must match for the file to be tested.'),
    '#default_value' => variable_get('pift_regex', '(\.diff|\.patch)$'),
    '#required' => TRUE,
  );
  // Defines project_issue issue status criteria.
  $form['criteria']['pift_status'] = array(
    '#type' => 'select',
    '#multiple' => TRUE,
    '#title' => t('Issue statuses'),
    '#description' => t('List of issue statuses that an issue must belong to one of in order to test files.'),
    '#default_value' => variable_get('pift_status', array()),
    '#options' => list_allowed_values($field),
    '#required' => TRUE,
  );
  // Advanced settings container.
  $form['advanced'] = array(
    '#type' => 'fieldset',
    '#title' => t('Advanced'),
    '#collapsible' => TRUE,
  );
  // Defines the 'base' project used in testing.
  $project = node_load(PIFT_PID);
  $form['advanced']['pift_pid'] = array(
    '#type' => 'textfield',
    '#title' => t('Core'),
    '#description' => t('Project that contains the core code required by modules to function.'),
    '#autocomplete_path' => 'project/autocomplete/issue/project',
    '#default_value' => ($project ? $project->title : ''),
    '#required' => TRUE,
  );
  // Defines the base URL location for the git repositories.
  $form['advanced']['pift_git_base_url'] = array(
    '#type' => 'textfield',
    '#title' => t('PIFT Git Base URL'),
    '#description' => t('Base URL for Git repositories, like git://git.drupal.org/project/'),
    '#default_value' => variable_get('pift_git_base_url', 'git://git.drupal.org/project/'),
    '#required' => TRUE,
  );


  $form['#submit'] = array('pift_admin_settings_form_submit');
  return system_settings_form($form);
}

/**
 * Form submission handler for pift_admin_settings_form().
 *
 * @see pift_admin_settings_form_validate()
 */
function pift_admin_settings_form_submit($form, &$form_state) {
  // Determine the project id, based on the node title.
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', project_project_node_types(), 'IN')
    ->propertyCondition('status', 1)
    ->propertyCondition('title', $form_state['values']['pift_pid'])
    ->range(0,1);
  $result = $query->execute();

  // Add the project node ID to the submitted form.
  if (isset($result['node'])) {
    $project = reset($result['node']);
    $form_state['values']['pift_pid'] = $project->nid;
  }
}

