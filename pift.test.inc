<?php

/**
 * @file
 * Provide test utility and database functions for the PIFT module.
 */

/**
 * Check the 'testable' criteria for the specified file.
 *
 * @param object $file
 *   The loaded file object to check.
 *
 * @return boolean
 *   Indicates whether the file passes the criteria check.
 */
function pift_test_check_criteria_file($file) {
  if ((!isset($file->filename)) || !preg_match('/' . variable_get('pift_regex', '(\.diff|\.patch)$') . '/', $file->filename)) {
    return FALSE;
  }
  return TRUE;
}

/**
 * Deletes any tests with the given 'id' value from the {pift_data} db table.
 *
 * @param integer $fid
 *   The unique file id whose test results are to be removed.
 * @param integer $type
 *   The type of tests to delete, PIFT_TYPE_FILE or PIFT_TYPE_BRANCH.
 */
function pift_test_delete_by_id($id, $type = PIFT_TYPE_FILE) {
  db_delete('pift_data')
    ->condition('type', $type)
    ->condition('id', $id)
    ->execute();
}

/**
 * Deletes any tests with the given 'nid' value from the {pift_data} db table.
 *
 * @param integer $nid
 *   The issue node id whose test results are to be removed.
 * @param integer $type
 *   The type of tests to delete, PIFT_TYPE_FILE or PIFT_TYPE_BRANCH.
 */
function pift_test_delete_by_nid($nid, $type = PIFT_TYPE_FILE) {
  db_delete('pift_data')
    ->condition('type', $type)
    ->condition('nid', $nid)
    ->execute();
}
