<?php

/**
 * @file
 * Provides nodechanges integration for the PIFT module.
 */

/**
 * Implements hook_nodechanges_file_changes_element_alter().
 *
 * Adds test status and results to the extended_file_field formatter output on
 * nodechanges file changes fields.
 */
function pift_nodechanges_file_changes_element_alter(&$element, $context) {
  // If there are no files, bail.
  if (empty($element['#files'])) {
    return;
  }

  $node = $context['entity'];
  $new_rows = array();

  // Hide status column.
  unset($element['#header'][0]);

  // Loop through each file row, adding the test results as necessary.
  foreach ($element['#rows'] as $fid => $row) {
    // Generate our replacement row items, maintaining any existing classes,
    // attributes, etc.
    if (!isset($row['data'])) {
      $new_rows[$fid] = array('data' => $row, 'class' => array('pift-file-info'));
    }
    else {
      $new_rows[$fid] = $row;
      $new_rows[$fid]['class'][] = 'pift-file-info';
    }

    // Hide status column.
    unset($new_rows[$fid]['data'][0]);

    $issue_wrapper = entity_metadata_wrapper('node', $node);
    if (!pift_test_check_criteria_file($element['#files'][$fid]) || !pift_ci_project_info($issue_wrapper->field_project->raw())) {
      // File is not testable, skip rummaging for results.
      continue;
    }

    // Add PIFT Test results.
    $file_tests = pift_load_tests($node, PIFT_CI_TYPE_FILE);
    $tests = pift_ci_display_tests($node, isset($file_tests[$fid]) ? $file_tests[$fid] : []);
    $new_rows[$fid . '-results'] = [
      'data' => [
        [
          'data' => drupal_render($tests),
          'colspan' => 2,
        ],
      ],
      'class' => ['pift-test-info'],
    ];
  }

  // Replace our table rows with the updated element.
  $element['#rows'] = $new_rows;
}
