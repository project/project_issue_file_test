<?php

use \GuzzleHttp\Exception\RequestException;
use \GuzzleHttp\Psr7;
use Symfony\Component\Yaml\Yaml;

/**
 * @file
 * Entity class definition for PiftCiJob class.
 */
class PiftCiJob extends Entity {

  /**
   * @var int Unique ID for this CI data entity.
   */
  public $job_id;

  /**
   * @var string Environment type for the test.
   */
  public $environment;

  /**
   * @var string Type of test, PIFT_CI_TYPE_*.
   */
  public $target_type;

  /**
   * @var string Related CI detail URL for results.
   */
  public $ci_url;

  /**
   * @var string Status of the test, PIFT_CI_STATUS_*.
   */
  public $status = PIFT_CI_STATUS_QUEUE;

  /**
   * @var string Result of the test, PIFT_CI_RESULT_*.
   */
  public $result = NULL;

  /**
   * @var string Summary message of test result.
   */
  public $message = 'Queueing';

  /**
   * @var int Issue node ID.
   */
  public $issue_nid;

  /**
   * @var int Related CI detail record ID user for retrieving results. Only used when type is PIFT_CI_TYPE_FILE.
   */
  public $file_id;

  /**
   * @var string Issue branch when type is PIFT_CI_TYPE_MERGE_REQUEST.
   */
  public $issue_branch = NULL;

  /**
   * @var string The branch that a merge request will be merged into.
   */
  public $merge_target = NULL;

  /**
   * @var int Release node ID.
   */
  public $release_nid;

  /**
   * @var string Core branch to test with. Only used when test is for a
   * non-core project.
   */
  public $core_branch = '';

  /**
   * @var string SHA1 of the commit that triggered this test. Used when type is
   *   PIFT_CI_TYPE_BRANCH or PIFT_CI_TYPE_MERGE_REQUEST.
   */
  public $commit_id;

  /**
   * @var int Number of times this build (or test) has been requested.
   */
  public $build_iteration = 1;

  /**
   * @var int indicating a jobs order in the testing queue.
   */
  public $job_priority;

  /**
   * @var int Timestamp when this record was updated.
   */
  public $updated = NULL;

  /**
   * @var int Timestamp when this record was created.
   */
  public $created = NULL;

  /**
   * @var int User ID of the user who created this job.
   */
  public $uid = NULL;

  /**
   * @var string Reason the job was created.
   */
  public $reason = '';

  /**
   * @var int User ID of the user who requested bypassing a failing branch.
   */
  public $bypass_uid = NULL;

  /**
   * @var bool Flag for whether a test was custom or standard.
   */
  public $custom = 0;

  /**
   * @var string The category indicator for this test: legacy/current
   */
  private $release_category = NULL;

  /**
   * @var string Extended build details, usually failure explanation.
   */
  public $build_details = '';

  /**
   * @var int Count of checkstyle messages. The actual messages may be
   *   agressively cleaned.
   */
  public $checkstyle = 0;

  /**
   * @var int If job result is a diff with another job, that job’s ID.
   */
  public $result_diff_job_id = 0;

  /**
   * @var array Files to save for followup comment.
   */
  private $files_to_save = [];

  /**
   * Creates a new entity.
   *
   * @see entity_create()
   */
  public function __construct(array $values = array()) {
    try {
      parent::__construct($values, 'pift_ci_job');
    }
    catch (Exception $e) {
      // Thats a really terrible use of an exception.
    }

    if (is_null($this->uid)) {
      $this->uid = $GLOBALS['user']->uid;
    }
  }

  /**
   * Load an PiftCiJob entity.
   *
   * @param int $job_id ID of the CI job to load.
   */
  public static function load($job_id) {
    return entity_load_single('pift_ci_job', $job_id);
  }

  /**
   * Save a pift_ci_job entity.
   *
   * @param bool $complete
   *   TRUE to update the issue & send email notifications as needed.
   * @param bool $preserve_timestamp
   *   TRUE to keep the existing timestamp of the job.
   *
   * @return
   * @throws \Exception
   */
  public function save($complete = FALSE, $preserve_timestamp = FALSE) {
    if (!$preserve_timestamp) {
      $this->updated = REQUEST_TIME;
    }

    return parent::save();
  }

  /**
   * Check if a test is actively running.
   */
  public function isRunning() {
    return $this->status === PIFT_CI_STATUS_QUEUE || $this->status === PIFT_CI_STATUS_SENT || $this->status === PIFT_CI_STATUS_RUNNING;
  }

  /**
   * Get the project node for this test's release branch.
   */
  public function getProject() {
    return node_load(project_release_get_release_project_nid(node_load($this->release_nid)));
  }

  /**
   * Get the release category for the test's release node.
   */
  public function getReleaseCategory() {
    if (!isset($this->release_category)) {
      $release_wrapper = entity_metadata_wrapper('node', node_load($this->release_nid));
      $this->release_category = $release_wrapper->field_release_category->value();
    }

    return $this->release_category;
  }

  /**
   * Get the file object for the test.
   */
  public function getFile() {
    if (!isset($this->file)) {
      $issue_node = node_load($this->issue_nid);
      foreach ($issue_node->field_issue_files[LANGUAGE_NONE] as $file) {
        if ($file['fid'] == $this->file_id) {
          $this->file = $file;
          break;
        }
      }
    }
    return $this->file;
  }

  /**
   * Get a test's title for humans.
   */
  public function getTitle() {
    $environment = $this->getEnvironment();
    $environment_title = $environment['title'];
    if (!empty($this->core_branch)) {
      $environment_title .= ', Drupal ' . check_plain($this->core_branch);
    }

    // Load project* nodes and set breadcrumbs.
    if (!empty($this->file_id)) {
      $file = $this->getFile();
      return t('<code>@file</code> test with @environment', [
        '@file' => drupal_basename($file['uri']),
        '@environment' => $environment_title,
      ]);
    }
    elseif ($this->target_type === PIFT_CI_TYPE_MERGE_REQUEST) {
      $merge_request = $this->getMergeRequestInfo();
      return t('Merge request !merge_request test with @environment', [
        '!merge_request' => l('!' . $merge_request['iid'], $merge_request['issue_fork']->mergeRequestUrl($merge_request['iid']), ['external' => TRUE]),
        '@environment' => $environment_title,
      ]);
    }
    else {
      return t('@branch test with @environment', [
        '@branch' => node_load($this->release_nid)->field_release_version[LANGUAGE_NONE][0]['value'],
        '@environment' => $environment_title,
      ]);
    }
  }

  /**
   * Get a test’s target’s title for humans.
   *
   * @return string
   *   The human-readable title of the test target.
   */
  public function getTargetTitle() {
    switch ($this->target_type) {
      case PIFT_CI_TYPE_BRANCH:
        return node_load($this->release_nid)->field_release_version[LANGUAGE_NONE][0]['value'];

      case PIFT_CI_TYPE_FILE:
        return drupal_basename($this->getFile()['uri']);

      case PIFT_CI_TYPE_MERGE_REQUEST:
        return t('merge request !@iid', ['@iid' => $this->getMergeRequestInfo()['iid']]);
    }
  }

  /**
   * Get the environment configuration for this job.
   */
  public function getEnvironment() {
    $environments = variable_get('pift_ci_environments');
    return $environments[$this->environment];
  }

  /**
   * When target_type is PIFT_CI_TYPE_MERGE_REQUEST, get the issue fork & IID.
   *
   * @return array
   *   - issue_fork: a DrupalorgIssueFork entity.
   *   - iid: the merge request IID.
   */
  public function getMergeRequestInfo() {
    $result = (new EntityFieldQuery())->entityCondition('entity_type', 'drupalorg_issue_fork')
      ->propertyCondition('nid', $this->issue_nid)
      ->propertyCondition('project_nid', entity_metadata_wrapper('node', node_load($this->release_nid))->field_release_project->raw())
      ->execute();
    $issue_fork = entity_load_single('drupalorg_issue_fork', array_keys($result['drupalorg_issue_fork'])[0]);
    return [
      'issue_fork' => $issue_fork,
      'iid' => $issue_fork->getMergeRequestIID($this->issue_branch, $this->merge_target),
    ];
  }

  /**
   * Get the project configuration for this job.
   */
  public function getProjectInfo() {
    return pift_ci_project_info(project_release_get_release_project_nid(node_load($this->release_nid)));
  }

  /**
   * Get currently-configured testing trigger, if any.
   */
  public function getCurrentTrigger() {
    if ($this->custom) {
      return NULL;
    }

    $project_info = $this->getProjectInfo();
    if (isset($project_info[$this->environment][$this->release_nid])) {
      foreach ($project_info[$this->environment][$this->release_nid] as $core_branch => $testing_trigger) {
        // Dereference the core branch label to compare to what the test ran
        // with.
        if (pift_get_current_core($core_branch, $this->getReleaseCategory()) === $this->core_branch) {
          return $testing_trigger;
        }
      }
    }

    return NULL;
  }

  /**
   * Check if a test should be blocked on branch passing.
   */
  public function checkBranchBlock() {
    // Only queued file tests can be blocked.
    if ($this->status !== PIFT_CI_STATUS_QUEUE || !in_array($this->target_type, [PIFT_CI_TYPE_FILE, PIFT_CI_TYPE_MERGE_REQUEST])) {
      return FALSE;
    }

    // Bypassing has been requested.
    if (!empty($this->bypass_uid)) {
      return FALSE;
    }

    // Only block if the environment & release is set to issue testing.
    if ($this->getCurrentTrigger() !== PIFT_CI_TEST_ISSUE) {
      return FALSE;
    }

    // Block if the branch has not passed.
    if ($branch_job = $this->getCurrentBranchJob()) {
      return $branch_job->result !== PIFT_CI_RESULT_PASS;
    }

    // The branch is missing, do not block.
    return FALSE;
  }

  /**
   * Get branch job at creation time with the issue job's environment & release.
   */
  public function getLastBranchJob() {
    $query = new EntityFieldQuery();
    $result = $query->entityCondition('entity_type', 'pift_ci_job')
      ->propertyCondition('target_type', PIFT_CI_TYPE_BRANCH)
      ->propertyCondition('status', [PIFT_CI_STATUS_COMPLETE, PIFT_CI_STATUS_SWEPT])
      ->propertyCondition('environment', $this->environment)
      ->propertyCondition('release_nid', $this->release_nid)
      ->propertyCondition('updated', $this->created, '<')
      ->propertyCondition('custom', FALSE)
      ->propertyOrderBy('job_id', 'DESC')
      ->range(0, 1)
      ->execute();
    if (isset($result['pift_ci_job'])) {
      return PiftCiJob::load(reset($result['pift_ci_job'])->job_id);
    }
  }

  /**
   * Get most recent branch job with the issue job's environment & release.
   */
  public function getCurrentBranchJob() {
    $query = new EntityFieldQuery();
    $result = $query->entityCondition('entity_type', 'pift_ci_job')
      ->propertyCondition('target_type', PIFT_CI_TYPE_BRANCH)
      ->propertyCondition('status', [PIFT_CI_STATUS_COMPLETE, PIFT_CI_STATUS_SWEPT])
      ->propertyCondition('environment', $this->environment)
      ->propertyCondition('release_nid', $this->release_nid)
      ->propertyCondition('custom', FALSE)
      ->propertyOrderBy('job_id', 'DESC')
      ->range(0, 1)
      ->execute();
    if (isset($result['pift_ci_job'])) {
      return PiftCiJob::load(reset($result['pift_ci_job'])->job_id);
    }
  }

  /**
   * Get all results in a structure for display.
   *
   * @param array $result_condition
   *   Optional result types to filter on.
   */
  public function getResultArray(array $result_condition = []) {
    $return = [];
    $query = db_select('pift_ci_job_result', 'r')
      ->condition('r.job_id', $this->job_id)
      ->fields('r', ['suite', 'class', 'test', 'result', 'error', 'output'])
      ->orderBy('r.job_result_id');
    if ($result_condition) {
      $query->condition('r.result', $result_condition);
    }
    $result = $query->execute();
    foreach ($result as $job_result) {
      $return[$job_result->suite][$job_result->class][$job_result->test] = $job_result;
    }
    if (!empty($this->result_diff_job_id)) {
      // Add in results from diff job.
      $query = db_select('pift_ci_job_result', 'r')
        ->condition('r.job_id', $this->result_diff_job_id)
        ->fields('r', ['suite', 'class', 'test', 'result', 'error', 'output'])
        ->orderBy('r.job_result_id');
      $query->leftJoin('pift_ci_job_result_diff', 'd', 'd.job_result_id = r.job_result_id AND d.job_id = :job_id', [':job_id' => $this->job_id]);
      $query->condition('d.job_result_id', NULL, 'IS NULL');
      if ($result_condition) {
        $query->condition('r.result', $result_condition);
      }
      $result = $query->execute();
      foreach ($result as $job_result) {
        if (!isset($return[$job_result->suite][$job_result->class][$job_result->test])) {
          $return[$job_result->suite][$job_result->class][$job_result->test] = $job_result;
        }
      }
    }

    return $return;
  }

}
